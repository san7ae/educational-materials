# Windows Setup

Install the following programs from their respective project websites:

* [Python 3.6](https://www.python.org/downloads/release/python-365/) (64 bit)
* [Git](https://www.python.org/downloads/release/python-365/) (64 bit)
* [Graphviz](https://graphviz.gitlab.io/_pages/Download/Download_windows.html)

The Git installer, will also provide Git Bash, which provides a Linux-style terminal to run commandline tools. Start it from the Windows Programs section.

*If you do not like working with Git Bash, alternatives are CMD and PowerShell, as well as the Windows Subsystem for Linux (WSL) on Windows 10. We do not provide support or instructions for these alternatives.*

## PATH Environment in Git Bash

Try to run Python in Bash:

```bash
python --version
```

If there is an error message `bash: python: command not found` the directory containing `Python.exe` is not registered in your `PATH` evironment variable.

Add your Python installation directories to `PATH` (the actual directories to `Python.exe` and `pip.exe` may vary on your system):

```bash
echo 'export PATH=${PATH}:${HOME}/AppData/Local/Programs/Python/Python36' >> ~/.bashrc
echo 'export PATH=${PATH}:${HOME}/AppData/Local/Programs/Python/Python36/Scripts' >> ~/.bashrc
source ~/.bashrc
```

Try again:

```bash
python --version
```

You should now be able to run `pip` as well:

```bash
pip --version
```

Use `pip` to install Python packages `pipenv` and `jupyterlab`:

```bash
pip install --user pipenv jupyterlab
```

Try to run the `jupyter` executable:

```bash
jupyter --version
```

If there is an error message `bash: jupyter: command not found` you also have to add Python's Script directory to your `PATH` (the actual directory may vary on your system):

```bash
echo 'export PATH=${PATH}:${HOME}/AppData/Roaming/Python/Python36/Scripts' >> ~/.bashrc
source ~/.bashrc
```

Try again:

```bash
jupyter --version
```

You should now be able to run `pipenv` as well:

```bash
pipenv --version
```

Then try running `dot`, which is a Graphviz program:

```bash
dot --version
```

If there is an error message `bash: dot: command not found` you also have to add the Graphviz bin directory to your `PATH` (the actual directory may vary on your system):

```bash
echo 'export PATH=${PATH}:"/c/Program Files (x86)/Graphviz2.38/bin"' >> ~/.bashrc
source ~/.bashrc
```

Try again:

```bash
dot --version
```

## Optional: Python CLI

If you are trying to run a Python commandline interpreter in Git Bash, the terminal may freeze. In this case, create an alias for Python to resolve this problem.

```bash
echo 'alias python="winpty python.exe"' >> ~/.bashrc
source ~/.bashrc
```

## Educational Materials

Now that your programming environment is working with Git Bash on Windows, follow the common instruction on the [home page](index.md#python-packages) to use the actual Jupyter notebooks from our educational-materials git repository.

Please note, that we do not regularly test the Jupyter notebooks on Windows. If you encounter a problem, please let us know and we will try to resolve it.
