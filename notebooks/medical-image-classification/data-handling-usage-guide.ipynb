{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Data Handling Usage Guide"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules)\n",
    "  * [Data](#Data)\n",
    "* [Teaching Content](#Teaching-Content)\n",
    "  * [CAMELYON Data Set](#CAMELYON-Data-Set)\n",
    "  * [The Preprocessing Package](#The-Preprocessing-Package)\n",
    "  * [Tiling](#Tiling)\n",
    "  * [Storing](#Storing)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Handling of huge slide images in the _tiff-format_ is very specific to the usecase of histology, requiring knowledge about specific packages like openslide. This series of notebooks offers some predefined classes for accessing the data, which will be presented here.\n",
    "\n",
    "These predefined classes and functions take away the need to become familiar with openslide and other specific packages, allow easy access to the data, but at the same time still show what steps are necessary, when dealing with huge images, which cannot be loaded into RAM at once.\n",
    "\n",
    "This notebook demonstrates how to use the `SlideManager` class to view Whole-Slide-Images (WSIs) and how to divide them into smaller pieces and how to store them for faster access.\n",
    "\n",
    "For demonstration purposes we suggest to only download one tumor-image (and corresponding annotation) and one normal-image of the CAMELYON16 dataset. Detailed instructions how to do this are in the notebook.\n",
    "\n",
    "The only thing you have to edit in this notebook is the path where these files are stored on your local machine.\n",
    "\n",
    "Just execute each cell and try to understand what happens. Of course you do not have to memorize everything now. When you have to use the `SlideManager` class yourself in another notebook, you can refer to this notebook as a usage guide."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "import numpy as np\n",
    "import h5py\n",
    "\n",
    "from skimage.filters import threshold_otsu\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "from preprocessing.datamodel import SlideManager\n",
    "from preprocessing.processing import split_negative_slide, split_positive_slide, create_tumor_mask, rgb2gray\n",
    "from preprocessing.util import TileMap\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "The data used in this notebook are from the CAMELYON data sets, which are freely available on the [CAMELYON data page](https://camelyon17.grand-challenge.org/Data/).\n",
    "\n",
    "The whole data sets have the following sizes:\n",
    "- CAMELYON16 *(~715 GiB)*\n",
    "- CAMELYON17 *(~2,8 TiB)*\n",
    "\n",
    "For this notebook to work the following file structure inside the data set folders must be given:\n",
    "\n",
    "```\n",
    "data\n",
    "├── CAMELYON16\n",
    "│   └── training\n",
    "│       ├── lesion_annotations\n",
    "│       │   └── tumor_001.xml - tumor_110.xml\n",
    "│       ├── normal\n",
    "│       │   └── normal_001.tif - normal_160.tif\n",
    "│       └── tumor\n",
    "│           └── tumor_001.tif - tumor_110.tif\n",
    "│\n",
    "└── CAMELYON17\n",
    "    └── training\n",
    "        ├── center_0\n",
    "        │   └── patient_000_node_0.tif - patient_019_node_4.tif\n",
    "        ├── center_1\n",
    "        │   └── patient_020_node_0.tif - patient_039_node_4.tif\n",
    "        ├── center_2\n",
    "        │   └── patient_040_node_0.tif - patient_059_node_4.tif\n",
    "        ├── center_3\n",
    "        │   └── patient_060_node_0.tif - patient_079_node_4.tif\n",
    "        ├── center_4\n",
    "        │   └── patient_080_node_0.tif - patient_099_node_4.tif\n",
    "        ├── lesion_annotations\n",
    "        │   └── patient_004_node_4.xml - patient_099_node_4.xml\n",
    "        └── stage_labels.csv\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IMPORTANT**\n",
    "    \n",
    "Not all of the files are needed to run this notebook. If you have not already downloaded the CAMELYON16 data set, download the following three files from [the official google drive](https://drive.google.com/drive/folders/0BzsdkU4jWx9Ba2x1NTZhdzQ5Zjg):\n",
    "- `normal/normal_101.tif` (2 GiB)\n",
    "- `tumor/tumor_110.tif` (1.4 GiB)\n",
    "- `tumor_110.xml` (in lesion_annoations.zip)\n",
    "\n",
    "Note that you will need the whole CAMELYON16 data set in the next notebook. So after you have downloaded the three mentioned files, you might already want to start the download of the complete CAMELYON16 data set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Teaching Content"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### CAMELYON Data Set\n",
    "\n",
    "The [CAMELYON Challenges](https://camelyon17.grand-challenge.org/) are two grand challenges in the year 2016 and 2017 organized by the Diagnostic Image Analysis Group (DIAG) with the goal to evaluate algorithms for automated detection and classification of breast cancer metastases in whole-slide images (WSI) histological lymph node sections.\n",
    "\n",
    "Each of the challenges provides a big [data set](https://camelyon17.grand-challenge.org/Data/) of Whole-Slide-Images (WSIs). Each slide contains one or more hematoxylin and eosin (H&E) stained sections of one lymph node. The CAMELYON17 data set alone consists of 100 patients for training and 100 patients for testing with each 5 slides. Additionally the data set provides lesion-level annotations for tumorous tissue in form of tumor masks for CAMELYON16 and annotations in XML-Files for CAMELYON17.\n",
    "\n",
    "The WSIs are provided in TIFF-Format. Each file consists of multiple layers with every layer providing different resolution of the image allowing fast access/viewing of multiple resolutions. The lowest layer (layer 0) has the original resolution and going up the resolution of each layer is halved. Effectively stacking up layers like a pyramid.\n",
    "\n",
    "The images on the lowest level of the slide can be as huge as over 2,000,000 Pixels per axis, which makes it difficult to load a whole image into the memory at once."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Preprocessing Package\n",
    "\n",
    "The `preprocessing` package provides classes and functions that allow preprocessing the CAMELYON data set in a comfortable way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### SlideManager\n",
    "\n",
    "The `SlideManager` class provides access to the whole-slide images of the CAMELYON data set. On object creation, it reads in the files in the specified path and creates `Slide` objects for every WSI.\n",
    "\n",
    "\n",
    "Following example loads in only the CAMELYON16 dataset, but loading the CAMELYON17 dataset is possible aswell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### EDIT THIS CELL:\n",
    "### Assign the path to your CAMELYON16 data and create the directories\n",
    "### if they do not exist yet.\n",
    "CAM_BASE_DIR = '/path/to/CAMELYON/data/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Do not edit this cell\n",
    "CAM16_DIR = CAM_BASE_DIR + 'CAMELYON16/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# for minimal data set:\n",
    "mgr = SlideManager(cam16_dir=CAM16_DIR)\n",
    "\n",
    "# for whole train set (CAMELYON16 and 17):\n",
    "# mgr = SlideManager(cam16_dir='some/path/CAMELYON16', cam17_dir='some/path/CAMELYON17')\n",
    "\n",
    "print(mgr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`SlideManager` objects offer multiple ways to access slides:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# annotated slides (slides with tumor annotation)\n",
    "slides_anno = mgr.annotated_slides\n",
    "print('Number of annotated slides: ', len(slides_anno))\n",
    "\n",
    "# negative slides\n",
    "slides_negative = mgr.negative_slides\n",
    "print('Number of positive slides: ', len(slides_negative))\n",
    "\n",
    "# get a random slide\n",
    "slide = random.choice(mgr.slides)\n",
    "print('\\nA random slide:')\n",
    "print(slide)\n",
    "\n",
    "# list two slide names\n",
    "print('\\nTwo random slide names:')\n",
    "for name in random.choices(mgr.slide_names, k=2):\n",
    "    print(' ', name)\n",
    "\n",
    "# choose a specific slide by name\n",
    "print('\\nSlide by name:')\n",
    "#print(mgr.get_slide('Normal_101'))\n",
    "print(mgr.get_slide('Normal_001'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Slides\n",
    "\n",
    "The `Slide` class describes a single WSI. It is based on the `OpenSlide` class but also provides information about stage (only for CAMELYON17 slides) and possible tumor annotations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "slide = random.choice(mgr.negative_slides)\n",
    "print('Name: {}\\n'\n",
    "      'Size on layer 0: {:,} × {:,} pixel\\n'\n",
    "      'Layers: {}\\n'\n",
    "      'State: {}\\n'\n",
    "      'Annotations: {}'.format(slide.name,\n",
    "                               *slide.level_dimensions[0], # openslide method\n",
    "                               slide.level_count,          # openslide method\n",
    "                               slide.stage,\n",
    "                               len(slide.annotations)))\n",
    "\n",
    "slide.get_thumbnail((300, 300))  # openslide method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "slide = random.choice(mgr.annotated_slides)\n",
    "print('Name: {}\\n'\n",
    "      'Size on layer 0: {:,} × {:,} pixel\\n'\n",
    "      'Layers: {}\\n'\n",
    "      'State: {}\\n'\n",
    "      'Annotations: {}'.format(slide.name,\n",
    "                               *slide.level_dimensions[0],  # open slide method\n",
    "                               slide.level_count,           # open slide method\n",
    "                               slide.stage,\n",
    "                               len(slide.annotations)))\n",
    "\n",
    "slide.get_thumbnail((300, 300))  # open slide method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Slide` method `get_full_slide()` allows a direct access to the slide as image on a specific level. For all further operations we define one `level`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "level = 0\n",
    "print('Slide dimensions on layer %i: '%level, slide.level_dimensions[level])\n",
    "img = slide.get_full_slide(level=5) # we cannot show it on level 0, would require ~100+ GByte RAM\n",
    "print('Image size:', img.size)\n",
    "plt.imshow(img)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Annotations\n",
    "\n",
    "Tumor annotations are represented by the `Annotation` class. Every `Annotation` object has a name and a polygon encompassing the tumor region. Annotated slides may have multiple `Annotation` objects.\n",
    "\n",
    "The `Annotation` object can create an image of a tumor region with the polygon drawn over. Here the first 3 annotations of the first annotated slide:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "slide = mgr.annotated_slides[0]\n",
    "print('Slide name: {}\\n'.format(slide.name))\n",
    "for annotation in slide.annotations[:3]:\n",
    "    bounds = annotation.get_boundaries(0)\n",
    "    print('Annotation name: \"{}\"\\n'\n",
    "          'Position on layer 0: {}\\n'\n",
    "          'Size on layer 0: {:,} × {:,} pixel\\n'.format(annotation.name,\n",
    "                                                        bounds[0],\n",
    "                                                        *bounds[1]))\n",
    "    plt.figure()\n",
    "    plt.imshow(annotation.get_image())\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tiling\n",
    "\n",
    "In general the file size and dimensions of a WSI are much bigger than a convolutional neural network can handle. To be able to predict the cancer status of tissue samples we need to cut each slide into small images, tiles, which we can feed into a network as training data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Negative slides\n",
    "\n",
    "Negative tissue slides are slides of healthy tissue without any tumorous cells. \n",
    "From these negative slides we want to extract as many tiles with tissue as possible and ignore those with only background.\n",
    "\n",
    "To split the background from the actual tissue data we can use an image processing algorithm based on the [Otsu Method](https://en.wikipedia.org/wiki/Otsu's_method) which is provided in the [scikit-image package](http://scikit-image.org/docs/dev/api/skimage.filters.html#skimage.filters.threshold_otsu). This method calculates a threshold that divides a gray scale image into foreground and background.\n",
    "\n",
    "As an example we can get a negative slide from the SlideManager:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "negative_slide = mgr.negative_slides[0]\n",
    "print('Slide name: \"{}\"\\n'\n",
    "      'Tumorous: {}'.format(negative_slide.name,\n",
    "                            negative_slide.has_tumor))\n",
    "negative_slide.get_thumbnail((300, 300))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can load the image into an NumPy array, convert it to gray scale and calculate the otsu threshold, though we will use another zoom level here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# load the slide into numpy array\n",
    "arr = np.asarray(negative_slide.get_full_slide(level=6))\n",
    "print('array shape:', arr.shape)\n",
    "\n",
    "# convert it to gray scale\n",
    "arr_gray = rgb2gray(arr)\n",
    "print('gray array shape:', arr_gray.shape)\n",
    "\n",
    "# calculate otsu threshold\n",
    "threshold = threshold_otsu(arr_gray)\n",
    "threshold"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The threshold splits the image into background and foreground. Pixels with a value lower are considered background and pixels with a higher value are considered foreground. To visualize this we can create a binary mask:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# create an otsu mask\n",
    "mask = arr_gray > threshold\n",
    "plt.imshow(mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we compare the mask with the slide thumbnail above we can see the algorithm successfully separated the pixels with tissue data from the background. The binary mask also allows us to count how many pixels of actual tissue data we have by summing it up:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "np.sum(mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To double check we can slice the mask into three vertical strips and sum them up:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Points of interests between 0 and 1700:    {:9,}\\n'\n",
    "      'Points of interests between 1700 and 4000: {:9,}\\n' \n",
    "      'Points of interests between 3400 and 5120: {:9,}'.format(np.sum(mask[...,0:1700]),\n",
    "                                                                np.sum(mask[...,1700:3400]),\n",
    "                                                                np.sum(mask[...,3400:])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see most off the tissue is in the first third and barely any in the last. This is exactly what we can see on the images above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This method to separate background from foreground by creating one binary mask works very well for smaller images and so far we have only been working on layer 5 of the WSI pyramid. But for lower layers with much higher dimension this step can be very memory intensive. To avoid this problem we can calculate the otsu threshold once and then create a mask based on that threshold for every tile of the slide individually.\n",
    "\n",
    "The function `split_negative_slide` of the *preprocessing* module splits a slide into tiles with the specified dimensions. For each tile a binary mask is created based on the otsu threshold. If the sum of this mask is higher than the provided threshold, the tile and its boundaries are returned."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tile_iter = split_negative_slide(\n",
    "    negative_slide, level=3,\n",
    "    otsu_threshold=threshold,  # otsu threshold calculated earlier\n",
    "    tile_size=256,\n",
    "    overlap=0,                 # no overlap\n",
    "    poi_threshold=0.9)         # only select tiles with at least 90% tissue\n",
    "\n",
    "# plot the first 3 tiles\n",
    "plt.figure(figsize=(20, 6))\n",
    "for i, (tile, bounds) in enumerate(tile_iter):\n",
    "    print(bounds)          # tile boundaries on layer 0\n",
    "    plt.subplot(1, 3, i+1)\n",
    "    plt.imshow(tile)       # plot the tile\n",
    "\n",
    "    if i >= 2:\n",
    "        break\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `TileMap` class can be used to display the tiles and see which areas of the slide are selected. It's `image` attribute is an image of the WSI with all tiles marked:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# initialize the map with the slide itself\n",
    "tm = TileMap(negative_slide)\n",
    "\n",
    "print('start splitting', datetime.datetime.now())\n",
    "# create a new and unconsumed tile iterator\n",
    "tile_iter = split_negative_slide(negative_slide, level=0,\n",
    "                                 otsu_threshold=threshold,\n",
    "                                 tile_size=512, \n",
    "                                 overlap=0,\n",
    "                                 poi_threshold=0.9)\n",
    "print('end splitting', datetime.datetime.now())\n",
    "\n",
    "for _, bounds in tile_iter:\n",
    "    # add the boundaries of every tile to the map\n",
    "    print('next tile', datetime.datetime.now())\n",
    "    tm.add_tile(bounds)\n",
    "\n",
    "tm.image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see the green rectangles which represent a tile are only covering the area with tissue. With the `poi_threshold` parameter we can set the minimum amount of tissue each tile has (here 20%) and with the `overlap` parameter we can let the tiles overlap each other."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Positive slides\n",
    "\n",
    "From positive slides we only want tiles with actual tumorous tissue to train our network. We can ignore the background and the rest of the tissue. We can use the same process we used for negative slides, but instead of a binary mask based on the otsu threshold, we can create a tumor masked based on the CAMELYON data set tumor annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "positive_slide = mgr.get_slide('tumor_110')\n",
    "print('Slide name: \"{}\"'.format(positive_slide.name))\n",
    "positive_slide.get_thumbnail((300, 300))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = create_tumor_mask(positive_slide, level=level)\n",
    "plt.imshow(mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `create_tumor_mask` takes the polygons from all annotations and creates a binary mask were $0$ represents healthy and $1$ tumorous tissue.\n",
    "\n",
    "*Note: Micro and macro metastases in the CAMELYON data set are annotated exhaustively. However, there might be isolated tumor cells (ITCs) that are not annotated at all. For this reason we only want to use the annotated areas from positive slides as positive tiles and we don't want to use the remaining tissue as negative tiles.*\n",
    "\n",
    "As with the otsu mask for negative slides, creating a tumor mask of a whole slide on a low layer might lead to memory issues. To prevent that we will only generate partial tumor mask for each tile we process.\n",
    "\n",
    "ToDo!\n",
    "\n",
    "\n",
    "\n",
    "The `split_positive_slide` function provides this. As an example we can again split a slide into $128\\times128$ tiles on layer 5 and use the `TileMap` to display the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tile_iter = split_positive_slide(\n",
    "    positive_slide, level=level,\n",
    "    tile_size=tile_size,  # tile size of 128×128 pixel\n",
    "    overlap=0)      # no overlap\n",
    "\n",
    "tm = TileMap(positive_slide)\n",
    "for _, bounds in tile_iter:\n",
    "    # add the boundaries of every tile to the map\n",
    "    tm.add_tile(bounds)\n",
    "\n",
    "print('Created {} tiles.'.format(len(tm.tiles)))\n",
    "tm.image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Storing\n",
    "\n",
    "Finally we need to be to store the preprocessed data in a way we can easily read out and feed into the CNN.\n",
    "\n",
    "The HDF5-format (Hierarchical Data Format version 5) is a data format designed to store and organize large amounts of data on the hard drive. For python the package [h5py](http://www.h5py.org/) offers a rich API around HDF5 that allows us to store and load sets of data very similar to the way we handle NumPy ndarrays.\n",
    "\n",
    "As an example, let us store one example slide into HDF5:\n",
    "\n",
    "First, we need to create a new tile iterator. This time we want all the tiles with at least 5% of tissue and a small overlap of 5 pixels. To be able to see which tiles are written we will make a new `TileMap` as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a new and unconsumed tile iterator\n",
    "tile_iter = split_negative_slide(negative_slide, level=5,\n",
    "                                 otsu_threshold=threshold,\n",
    "                                 tile_size=256, overlap=5,\n",
    "                                 poi_threshold=0.05)\n",
    "\n",
    "# initialize map\n",
    "tm = TileMap(negative_slide)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "filename = '{}/just_a_testfile_can_be_deleted.hdf5'.format(CAM16_DIR, negative_slide.name, level)\n",
    "label = 0                                              #  1\n",
    "\n",
    "with h5py.File(filename, \"w\", libver='latest') as h5:  #  2\n",
    "    # creating a date set in the file\n",
    "    dset = h5.create_dataset('data',                   #  3                      \n",
    "        (0, tile_size, tile_size, 3),                              #  4\n",
    "        dtype=np.uint8,                                #  5\n",
    "        maxshape=(None, tile_size, tile_size, 3),                  #  6\n",
    "        compression=\"lzf\")                             #  7\n",
    "    \n",
    "    lset = h5.create_dataset('label', (0,),            #  8\n",
    "                             maxshape=(None,),\n",
    "                             dtype=np.uint8)\n",
    "    cur = 0\n",
    "    for tile, bounds in tile_iter:                     #  9\n",
    "        dset.resize(cur + 1, axis=0)                   # 10 \n",
    "        dset[cur:cur + 1] = tile                       # 11 \n",
    "\n",
    "        lset.resize(cur + 1, axis=0)                   # 12\n",
    "        lset[cur:cur + 1] = label\n",
    "\n",
    "        cur += 1\n",
    "        tm.add_tile(bounds)                            # 13\n",
    "        print('.', end='')\n",
    "\n",
    "print('\\n{} tiles written'.format(cur))\n",
    "tm.image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Issue a shell command to see how many bytes have been written."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -l {filename} | cut -d' ' -f5,9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can see what has been written to the file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with h5py.File(filename, 'r') as h5:\n",
    "    for key in h5.keys():\n",
    "        print('Datasets \"{}\", shape: {}'.format(key, h5[key].shape))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numbers match, we can read out five random tiles and their labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classes = ('negative', 'positive')\n",
    "\n",
    "plt.figure(figsize=(20, 6))\n",
    "with h5py.File(filename, 'r') as h5:\n",
    "    rand_indexes = random.sample(range(len(h5['data'])), k=5)\n",
    "    for i, idx in enumerate(rand_indexes):\n",
    "        plt.subplot(1, 5, i+1)\n",
    "        plt.imshow(h5['data'][idx])\n",
    "        plt.title(classes[h5['label'][idx]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could successfully save and load the tiles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "We explored the CAMELYON data set and were able to use utilize different tools to extract interesting data from Whole-Slide-Images.\n",
    "\n",
    "The next step is to process all CAMELYON16 slides and save them into HDF5 format as a single file.\n",
    "\n",
    "It is also possible to add additional steps into the preprocessing, like color normalization to counter the different hues produced by different scanners and data augmentation to increase the volume of data to feed into the CNN even further."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "XXX<br/>\n",
    "by Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
