{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Preprocessing the Dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules)\n",
    "  * [Data](#Data)\n",
    "* [Dataset Generation](#Dataset-Generation)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook you will create your own dataset based on the CAMELYON16 data set solely (~700 giga bytes), as you should not need to download over 3.5 tera bytes (CAMELYON16 and 17 data set). Subsequent notebooks will use this data set you will create now. Once you have finished this series of notebooks you can proceed enhancing your implementation to the whole data set including the CAMELYON17 data set.\n",
    "\n",
    "The purpose of the preprocessing is the following:\n",
    "\n",
    "If we had enough RAM to store the whole data set, we would just load it once at the beginning of the training. But this is not the case. Reading the different WSI-files in their compressed tiff format every single time we train a new batch is very time consuming. So storing tiles with a fixed zoom level, fixed size, cropped and labeled in one single file, will save us a lot of time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Python Standard Library\n",
    "import random\n",
    "\n",
    "# External Modules\n",
    "import numpy as np\n",
    "import h5py\n",
    "from datetime import datetime\n",
    "from skimage.filters import threshold_otsu\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "# Furcifar Modules\n",
    "from preprocessing.datamodel import SlideManager\n",
    "from preprocessing.processing import split_negative_slide, split_positive_slide, create_tumor_mask, rgb2gray\n",
    "from preprocessing.util import TileMap\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "The data used in this notebook are from the CAMELYON data sets, which are freely available on the [CAMELYON data page](https://camelyon17.grand-challenge.org/Data/).\n",
    "\n",
    "The whole data sets have the following sizes:\n",
    "- CAMELYON16 *(~715 GiB)*\n",
    "- CAMELYON17 *(~2,8 TiB)*\n",
    "\n",
    "For this notebook to work the following file structure (for CAMELYON16) inside the data folder must be given:\n",
    "\n",
    "```\n",
    "data\n",
    "├── CAMELYON16\n",
    "│   ├── training\n",
    "│   │   ├── lesion_annotations\n",
    "│   │   │   └── tumor_001.xml - tumor_110.xml\n",
    "│   │   ├── normal\n",
    "│   │   │   └── normal_001.tif - normal_160.tif\n",
    "│   │   └── tumor\n",
    "│   │       └── tumor_001.tif - tumor_110.tif\n",
    "│   └── test\n",
    "│       ├── lesion_annotations\n",
    "│       │   └── test_001.xml - tumor_110.xml\n",
    "│       └── images\n",
    "│           └── test_001.tif - normal_160.tif\n",
    "│\n",
    "└── CAMELYON17\n",
    "    └── training\n",
    "        ├── center_0\n",
    "        │   └── patient_000_node_0.tif - patient_019_node_4.tif\n",
    "        ├── center_1\n",
    "        │   └── patient_020_node_0.tif - patient_039_node_4.tif\n",
    "        ├── center_2\n",
    "        │   └── patient_040_node_0.tif - patient_059_node_4.tif\n",
    "        ├── center_3\n",
    "        │   └── patient_060_node_0.tif - patient_079_node_4.tif\n",
    "        ├── center_4\n",
    "        │   └── patient_080_node_0.tif - patient_099_node_4.tif\n",
    "        ├── lesion_annotations\n",
    "        │   └── patient_004_node_4.xml - patient_099_node_4.xml\n",
    "        └── stage_labels.csv\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task:**\n",
    " \n",
    " If you have not done so far, download all remaining data of the CAMELYON16 data set and store it in a folder structure shown above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset Generation\n",
    "\n",
    "In this notebook, we will use parts of the [data-handling-usage-guide.ipynb](./data-handling-usage-guide.ipynb) to create our own dataset. You have two options:\n",
    "\n",
    "### Option A\n",
    "\n",
    "- Process all files from the CAMELYON16 data set\n",
    "- Slide zoom level 2 (0-9, 0 beeing the highest zoom)\n",
    "- Tile_size of 256x256\n",
    "- No overlap for negative tiles\n",
    "- 128 pixel overlap for tumorous (positive tiles) since they are scarce\n",
    "- Minimum of 20% tissue in tiles for normal slides\n",
    "- Minimum of 20% tumorours tissue for positive slides\n",
    "- We will get meaningful results in Part 1 of the tutorial  (CAMELYON16)\n",
    "- Processing will take approximately ~20 hours [\\*]\n",
    "- Training of CNN in the next Notebook will take ~10 hours [\\*]\n",
    "\n",
    "### Option B\n",
    "\n",
    "- Process only part of the CAMELYON16 data set\n",
    "- Slide zoom level 3 (0-9, 0 beeing the highest zoom)\n",
    "- Tile_size of 256x256\n",
    "- No overlap for negative tiles\n",
    "- No overlap for positive tiles\n",
    "- Minimum of 20% tissue in tiles for normal slides\n",
    "- Minimum of 20% tumorours tissue for positive slides\n",
    "- We will probably not get meaningful results in Part 1 of the tutorial  (CAMELYON16)\n",
    "- Processing will take approximately ~5 hours [\\*]\n",
    "- Training of CNN in the next Notebook will take ~2 hours [\\*]\n",
    "\n",
    "**Remark:**\n",
    "- [\\*] *[Tested on Xeon1231v3 @3.8Ghz, 16GB DDR3 @1666Hz]*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "Most importantly, we will save all tiles from all WSIs into a single HDF5 file. This is crucial because when accessing the data later for training, most time is consumed when opening a file. Additionally, the training works better, when a single batch (e.g. 100 tiles), is as heterogenous as the original data. So when we want to read 100 random tiles, we ideally want to read 100 tiles from 100 different slides and we do not want to open 100 different files to do so.\n",
    "\n",
    "**Background Information:**\n",
    "\n",
    "Depending on the staining process and the slide scanner, the slides can differ quite a lot in color. Therefore a batch containing 100 tiles from one slide only will most likely prevent the CNN from generalizing well.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### EDIT THIS CELL:\n",
    "### Assign the path to your CAMELYON16 data and create the directories\n",
    "### if they do not exist yet.\n",
    "CAM_BASE_DIR = '/path/to/CAMELYON/data/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Do not edit this cell\n",
    "CAM16_DIR = CAM_BASE_DIR + 'CAMELYON16/'\n",
    "GENERATED_DATA = CAM_BASE_DIR + 'tutorial/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "mgr = SlideManager(cam16_dir=CAM16_DIR)\n",
    "n_slides= len(mgr.slides)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### Execute this cell for option A\n",
    "\n",
    "level = 0\n",
    "tile_size = 512\n",
    "\n",
    "poi_percent = 90\n",
    "poi = poi_percent/100.\n",
    "poi_tumor = 0.99\n",
    "\n",
    "overlap = 0\n",
    "overlap_tumor = 256"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### Execute this cell for option B\n",
    "level = 3\n",
    "tile_size = 256\n",
    "poi_percent = 20\n",
    "poi = poi_percent/100.\n",
    "overlap_tumor = 0,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = '{}/{}x{}_poi{}_l{}.hdf5'.format(GENERATED_DATA, tile_size, tile_size, \n",
    "                                                       poi_percent, level)\n",
    "\n",
    "h5 = h5py.File(filename, \"w\", libver='latest')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tiles_neg = 0\n",
    "\n",
    "for i in range(len(mgr.negative_slides)): \n",
    "    # load the slide into numpy array\n",
    "    arr = np.asarray(mgr.negative_slides[i].get_full_slide(level=3))\n",
    "    # convert it to gray scale\n",
    "    arr_gray = rgb2gray(arr)\n",
    "\n",
    "    # calculate otsu threshold\n",
    "    threshold = threshold_otsu(arr_gray)\n",
    "    \n",
    "    # create a new and unconsumed tile iterator\n",
    "    # because we have so many  negative slides we do not use overlap\n",
    "    tile_iter = split_negative_slide(mgr.negative_slides[i], level=level,\n",
    "                                     otsu_threshold=threshold,\n",
    "                                     tile_size=tile_size, overlap=0,\n",
    "                                     poi_threshold=poi)\n",
    "\n",
    "    # creating a date set in the file\n",
    "    dset = h5.create_dataset(mgr.negative_slides[i].name, \n",
    "                             (0, tile_size, tile_size, 3), \n",
    "                             dtype=np.uint8,\n",
    "                             maxshape=(None, tile_size, tile_size, 3),\n",
    "                             compression=0)   \n",
    "\n",
    "    cur = 0\n",
    "    for tile, bounds in tile_iter:\n",
    "        if cur > 100: break\n",
    "        dset.resize(cur + 1, axis=0)\n",
    "        dset[cur:cur + 1] = tile\n",
    "        tiles_neg += 1\n",
    "        cur += 1 \n",
    "\n",
    "    print(datetime.now(), i, '/', len(mgr.negative_slides), '  tiles  ', cur)\n",
    "    print('neg tiles total: ', tiles_neg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tiles_pos = 0\n",
    "\n",
    "for i in range(len(mgr.annotated_slides)):\n",
    "    # create a new and unconsumed tile iterator\n",
    "    # because we have too litle positive slides, we use overlap half overlapping tilesize\n",
    "    tile_iter = split_positive_slide(mgr.annotated_slides[i], level=level,\n",
    "                                     tile_size=tile_size, overlap=overlap_tumor,\n",
    "                                     poi_threshold=poi_tumor) \n",
    "\n",
    "    # creating a date set in the file\n",
    "    dset = h5.create_dataset(mgr.annotated_slides[i].name, \n",
    "                             (0, tile_size, tile_size, 3), \n",
    "                             dtype=np.uint8,\n",
    "                             maxshape=(None, tile_size, tile_size, 3),\n",
    "                             compression=0)   \n",
    "\n",
    "    cur = 0\n",
    "    for tile, bounds in tile_iter:\n",
    "        if cur > 100: break\n",
    "        dset.resize(cur + 1, axis=0)\n",
    "        dset[cur:cur + 1] = tile\n",
    "        cur += 1\n",
    "        tiles_pos += 1\n",
    "        \n",
    "    print(datetime.now(), i, '/', len(mgr.annotated_slides), '  tiles  ', cur)\n",
    "    print('pos tiles total: ', tiles_pos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in list(h5.keys()):\n",
    "    if 'umor' in k:\n",
    "        if h5[k].shape[0] == 0:\n",
    "            print(k)\n",
    "            print(h5[k].shape)\n",
    "            del h5[k]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h5.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "The next step is to train a neural network with the preprocessed data to be able to classify and predict unseen tiles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "XXX<br/>\n",
    "by Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
