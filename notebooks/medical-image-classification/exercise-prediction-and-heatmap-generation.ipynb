{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Prediction and Heatmap Generation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules)\n",
    "* [Teaching Content](#Teaching-Content)\n",
    " * [Evaluation of the CAMELYON16 Challenge](#Evaluation-of-the-CAMELYON16-Challenge)\n",
    " * [Towards CAMELYON17](#Towards-CAMELYON17)\n",
    " * [Setting the Paths](#Setting-the-Paths)\n",
    " * [Loading the Model](#-Loading-the-Model)\n",
    " * [Reading-CAMELYON16-Test-Dataset](#Reading-CAMELYON16-Test-Dataset)\n",
    "* [Exercise](#Exercise)\n",
    " * [Normalization](#Normalization)\n",
    " * [Heatmap Generation](#Heatmap-Generation)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Now that we have a trained (and saved) model, we can use it to predict the slides of the CAMELYON16 test dataset. From the prediction of the individual tiles, we can build a heatmap of the whole slide, showing the regions, which are predicted to be metastatic. The steps in this notebook can be broken down into:\n",
    "- Load the trained model\n",
    "- Load CAMEYLON16 test dataset with Slidemanager\n",
    "- Get slides with `Slidemanager.get_test_slides` \n",
    "- Get tiles with `split_negative_slide`\n",
    "- Predict the tiles and build the heatmaps\n",
    "- Visually compare your heatmaps with the tumor masks (if test slides have metastatic regions)\n",
    "\n",
    "**Note:**\n",
    "\n",
    "Chances are high your model will not be able to produce good enough heatmaps. Therefore in the next notebook you will be offered high quality heatmaps produced by a far superior CNN."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow import keras\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import random\n",
    "import h5py\n",
    "import math\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from preprocessing.datamodel import SlideManager\n",
    "from preprocessing.processing import split_negative_slide, split_positive_slide, create_tumor_mask, rgb2gray, create_otsu_mask_by_threshold\n",
    "from preprocessing.util import TileMap\n",
    "from cnn.tissuedataset import TissueDataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Teaching Content\n",
    "\n",
    "### Evaluation of the CAMELYON16 Challenge\n",
    "\n",
    "Following the original CAMELYON16 challange, the task would now be, to predict CAMELYON16 test dataset. Back in 2016, the labels were not published to the public. The metrics to evaluate the model were:\n",
    "\n",
    "1) Receiver operating characteristic (ROC) at slide level and then calculate the are under the ROC curve (AUC).\n",
    "\n",
    "2) Free-response receiver operating characteristic (FROC) for lesion based evaluation. Briefly, this metric measures, how well the regions in a tumorus slide match the true regions. Also, for each coordinate in the metastatic region, a confidence score had to be submitted.\n",
    "\n",
    "If you are interested in evaluating your model and see how it would have performed in the CAMELYON16 Challenge you can read more about the evaluation and the scoring at the [official CAMELYON16 website](https://camelyon16.grand-challenge.org/Evaluation/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Towards CAMELYON17\n",
    "\n",
    "Since the labels of the CAMELYON16 challange have already been published it is no longer possible to hand in any results. Therefor we will not go into detail evaluating the model for the CAMELYON16 challange.\n",
    "\n",
    "Instead we will head straight towards the CAMELYON17 challenge. The second goal of CAMELYON16 (lesion based) also prepares for this. From the confidence score it is straight forward to create a heatmap as prediction for a slide (similar to the tumor mask). These heatmaps can then be used to achieve the goals of the CAMELYON17 challenge, which are:\n",
    "- Predict if a slide contains no tumor regions, only isolated tumor cells (ITCs), micro metastasis of macro metastasis.\n",
    "- To be able to achieve this, the CAMELYON17 dataset is labeled with 4 different classes.\n",
    "\n",
    "In the next notebooks, we will use the heatmaps, created with our model, to accomplish this. So the task in this notebook is to create the heatmaps first."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting the Paths\n",
    "\n",
    "Set the paths according the destination where you store the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### EDIT THIS CELL:\n",
    "### Assign the path to your CAMELYON16 data and create the directories\n",
    "### if they do not exist yet.\n",
    "CAM_BASE_DIR = '/path/to/CAMELYON/data/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Do not edit this cell\n",
    "CAM16_DIR = CAM_BASE_DIR + 'CAMELYON16/'\n",
    "GENERATED_DATA = CAM_BASE_DIR + 'tutorial/'\n",
    "MODEL_FINAL = GENERATED_DATA + 'model_final.hdf5'\n",
    "\n",
    "# Destination to store the heatmaps which we will create in this notebook\n",
    "HEATMAPS_CAM16_TESTSET = GENERATED_DATA +'test_set_predictions/'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading the Model\n",
    "\n",
    "First we will load our trained and saved model. Since we did not train the model with an optimizer from the `tf.keras` package, we will have to recompile it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Recreate the exact same model, including weights\n",
    "model = tf.keras.models.load_model(MODEL_FINAL)\n",
    "\n",
    "#model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=0.0005), \n",
    "#              loss='binary_crossentropy',\n",
    "#              metrics=['accuracy'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading CAMELYON16 Test Dataset\n",
    "\n",
    "The main purpose of creating a training dataset as a single HDF5 file was to reduce the time reading the data. This was crucial for training, because we needed to read the same data over and over again while training. Concerning the test dataset, this is not as crucial, because we only need to read every slide once after the training is finished.\n",
    "\n",
    "So to read the CAMELYON16 test dataset, we can just use the `SlideManager` class, `SlideManager.test_slides` attribute and the `split_annotated_slides` and `split_negative_slides` methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mgr = SlideManager(cam16_dir=CAM16_DIR)\n",
    "\n",
    "level = 0\n",
    "tile_size = 256\n",
    "poi_threshold = 0.9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "slide = mgr.get_slide('Test_001')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(slide)\n",
    "print(slide.dimensions)\n",
    "print(slide.level_dimensions[level])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we pass a test slide as parameter to the method `create_tumor_mask`, a mask will always be returned. If there exists no annotation xml file (because it is a slide without metastatic regions), the mask will just be blank. This method can be used to manually compare your generated heatmaps with the true tumor area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = create_tumor_mask(slide, level=8)\n",
    "print(mask.shape)\n",
    "plt.imshow(mask, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "### Normalization\n",
    "\n",
    "Since we trained our model with normalized images, we will also need the mean and the standard deviation of the color channels we used. \n",
    "\n",
    "**Task:**\n",
    "\n",
    "Create both varibles `mean_pixel` and `std_pixel` and assign the values by just looking them up in the last notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Exercise: Look up the corresponding values and save them into variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heatmap Generation\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Use your trained model to predict the individual tiles of each slide in the test dataset. From the predictions of your model (values form 0.0 to 1.0) build a heatmap for each slide. It should have the same ratio of width and height as the original slide, but of course with a smaller scale.\n",
    "\n",
    "**Hints:**\n",
    "\n",
    "- Use `split_negative_slides` on the test slides to receive the slides (you do not know if it is a tumor or normal slide). For the usage, refer to [data-handling-usage-guide.ipynb](./data-handling-usage-guide.ipynb).\n",
    "- When you use overlapping slides, the resoluton of you heatmap will be bigger. E.g. overlap of 128 to double the resolution.\n",
    "- Save your created heatmaps as png files (e.g. *test_001.png*)\n",
    "- Optional: Save the original (*.xml files) masks as images so you can compare them with your heatmaps.\n",
    "\n",
    "Here are examples of some created heatmaps (top: heatmaps. bottom: true masks from xml files in `CAMELYON17/test/lesion_annotations/`):\n",
    "\n",
    "<img src=\"https://gitlab.com/deep.TEACHING/educational-materials/raw/dev/media/klaus/camelyon16_heatmaps_created_examples.png\" width=\"512\" alt=\"internet connection needed\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Notes:**\n",
    "\n",
    "- This will take a lot of time (~20+ hours)\n",
    "- Generated Heatmaps will probably not be of good quality for different reasons:\n",
    " - Low zoom (2 of 0-9, 0 being highest zoom)\n",
    " - Only one zoom level used\n",
    " - No data augmentation\n",
    " - Inferior CNN architecture\n",
    " - Weak color normalization\n",
    " - Weak hyperparameter optimization\n",
    "- So there comes a lot together. But to go through this tutorial you should not need to be a deep learning expert and not need a 1000 euro GPU.\n",
    "- If you do not have the time to classify all tiles of all slides, you can just implement the code, run it to produce the first 5-10 heatmaps and proceed with the next notebook. \n",
    "- In the next notebook you will be offered some high quality heatmaps, produced with all the missing things, which were mentioned here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Exercise. Your code below"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "So far we have accomplished to ...\n",
    "- ... divide our huge data set into smaller pieces (tiles) to be even able to handle it and use it to train model.\n",
    "- ... build and train a CNN to predict whether a single tile contains metastatic or normal tissue.\n",
    "- ... use our CNN to predict the individual tiles of the slides of the test set.\n",
    "- ... put the predictions of a slide together in order to generate a heatmap (or mask), which looks similar to the masks provided.\n",
    "\n",
    "In the next notebook we will extract geometric features of these heatmaps to train another classifier, which will then be able to predict the tumor class of the slides (_negative, itc, micro, macro_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"XXX\"></a>[XXX]\n",
    "        </td>\n",
    "        <td>\n",
    "            text\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "XXX<br/>\n",
    "by Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
