var searchData=
[
  ['set_5fparam',['set_param',['../classdp_1_1_node.html#a0063bbec6419bf1acf97569af560f5ee',1,'dp.Node.set_param()'],['../classdp_1_1_model.html#ae20bd11912dfdacba183367a35609f9a',1,'dp.Model.set_param()']]],
  ['sigmoid',['sigmoid',['../classdp_1_1_node.html#a2c998eeb0dab9efb435996d7943c140c',1,'dp::Node']]],
  ['softmax',['softmax',['../classdp_1_1_node.html#a6fc94b2c0f7e0607d393e1cc20b60a5b',1,'dp::Node']]],
  ['sqrt',['sqrt',['../classdp_1_1_node.html#ade7baf82cb9df81b3277e5838ad3f019',1,'dp::Node']]],
  ['square',['square',['../classdp_1_1_node.html#a029c1d060077ae974efab8c894af613a',1,'dp::Node']]],
  ['sum',['sum',['../classdp_1_1_node.html#ab206d2efe09262a684bef9dbe0561163',1,'dp::Node']]]
];
