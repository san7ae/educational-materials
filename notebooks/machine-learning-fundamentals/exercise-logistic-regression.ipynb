{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "# ML-Fundamentals - Logistic Regression and Regularization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge) \n",
    "  * [Modules](#Python-Modules)\n",
    "* [Exercises - Multivariate Linear Regression](#Exercises---Multivariate-Linear-Regression)\n",
    "  * [Data Generation](#Data-Generation)\n",
    "  * [Logistic Function](#Logistic-Function)\n",
    "  * [Cross-Entropy](#Cross-Entropy)\n",
    "  * [Loss Function](#Loss-Function)\n",
    "  * [Cost Function](#Cost-Function)\n",
    "  * [Gradient Descent](#Gradient-Descent)\n",
    "  * [Training and Evaluation](#Training-and-Evaluation)\n",
    "      * [Plot Data and Decision Boundary](#Plot-Data-and-Decision-Boundary)\n",
    "      * [Accuracy](#Accuracy)\n",
    "  * [Regularization](#Regularization)\n",
    "  * [Proof - Pen&Paper](#Proof---Pen&Paper)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Literature](#Literature) \n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "In this exercise you will implement the _logistic regression_. Opposed to the _linear regression_, the purpose of this model is not to predict a continuous value (e.g. the temperature tomorrow), but to predict a certain class: For example, whether it will rain tomorrow or not. During this exercise you will:\n",
    "\n",
    "\n",
    "1. Implement the logistic function and plot it\n",
    "3. Implement the hypothesis using the logistic function\n",
    "4. Write a function to calculate the cross-entropy cost\n",
    "5. Implement the loss function using the hypothesis and cost\n",
    "6. Implement the gradient descent algorithm to train your model (optimizer) \n",
    "7. Visualize the decision boundary together with the data\n",
    "8. Calculate the accuracy of your model\n",
    "9. Extend your model with regularization\n",
    "10. Calculate the gradient for the loss function with cross-entropy cost (pen&paper)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Requirements\n",
    "### Knowledge\n",
    "\n",
    "You should have a basic knowledge of:\n",
    "- Logistic regression\n",
    "- Cross-entropy loss\n",
    "- Gradient descent\n",
    "- numpy\n",
    "- matplotlib\n",
    "\n",
    "Suitable sources for acquiring this knowledge are:\n",
    "- [Logistic Regression Notebook](http://christianherta.de/lehre/dataScience/machineLearning/basics/logistic-regression.php) by Christian Herta and corresponding [lecture slides](http://christianherta.de/lehre/dataScience/machineLearning/logisticRegression.pdf) (German)\n",
    "- [Regularization Notebook](http://christianherta.de/lehre/dataScience/machineLearning/basics/regularization.php) by Christian Herta and corresponding [lecture slides](http://christianherta.de/lehre/dataScience/machineLearning/regularization.pdf) (German)\n",
    "- Chapter 5.1 of [Deep Learning](http://www.deeplearningbook.org/contents/ml.html) by Ian Goodfellow \n",
    "- Some parts of chapter 1 and 3 of [Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/people/cmbishop/#!prml-book) by Christopher M. Bishop\n",
    "- [numpy quickstart](https://docs.scipy.org/doc/numpy-1.15.1/user/quickstart.html)\n",
    "- [Matplotlib tutorials](https://matplotlib.org/tutorials/index.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "### Python Modules\n",
    "\n",
    "By [deep.TEACHING](https://www.deep-teaching.org/) convention, all python modules needed to run the notebook are loaded centrally at the beginning. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Exercise - Logistic Regression\n",
    "\n",
    "For convenience and visualization, we will only use two features in this notebook, so we are still able to plot them together with the target class. But your implementation should also be capable of handling more (except the plots). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Generation\n",
    "\n",
    "First we will create some artificial data. For each class, we will generate the features with bivariate (2D) normal distribution;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# class 0:\n",
    "# covariance matrix and mean\n",
    "cov0 = np.array([[5,-4],[-4,4]])\n",
    "mean0 = np.array([2.,3])\n",
    "# number of data points\n",
    "m0 = 1000\n",
    "\n",
    "# class 1\n",
    "# covariance matrix\n",
    "cov1 = np.array([[5,-3],[-3,3]])\n",
    "mean1 = np.array([1.,1])\n",
    "# number of data points\n",
    "m1 = 1000\n",
    "\n",
    "# generate m gaussian distributed data points with\n",
    "# mean and cov.\n",
    "r0 = np.random.multivariate_normal(mean0, cov0, m0)\n",
    "r1 = np.random.multivariate_normal(mean1, cov1, m1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plt.scatter(r0[...,0], r0[...,1], c='b', marker='o', label=\"class 0\")\n",
    "plt.scatter(r1[...,0], r1[...,1], c='r', marker='x', label=\"class 1\")\n",
    "plt.xlabel(\"x0\")\n",
    "plt.ylabel(\"x1\")\n",
    "plt.legend()\n",
    "plt.show()\n",
    "\n",
    "X = np.concatenate((r0,r1))\n",
    "y = np.zeros(len(r0)+len(r1))\n",
    "y[:len(r0),] = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Logistic Function\n",
    "\n",
    "For the logistic regression, we want the output of the hypothesis to be in the interval $]0, 1[$. This is done using the _logistic function_. The logistic function is a special case of the _sigmoid function_, though in the domain of machine learning, the term _sigmoid function_ is often used as a synonym for _logistic function_:\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the _logistic function_ and plot it for 1000 points in the interval of $[-10,10]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def logistic_function(x):\n",
    "    \"\"\" Applies the logistic function to x, element-wise. \"\"\"\n",
    "    raise NotImplementedError(\"You should implement this function\")\n",
    "\n",
    "### Insert code to plot the logistic function below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Logistic Hypothesis\n",
    "\n",
    "The logistic hypothesis is defined as:\n",
    "\n",
    "$$\n",
    "h_\\theta(\\vec x) = sigmoid(\\vec \\theta^T \\vec x')\n",
    "$$\n",
    "\n",
    "with:\n",
    "\n",
    "$$\n",
    "\\vec x = \\begin{pmatrix} \n",
    "x_1 & x_2 & \\ldots & x_n \\\\\n",
    "\\end{pmatrix}\n",
    "\\text{   and   }\n",
    "\\vec x' = \\begin{pmatrix} \n",
    "1 & x_1 & x_2 & \\ldots & x_n \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "or for the whole data set $X$ and $X'$\n",
    "\n",
    "$$\n",
    "X = \\begin{pmatrix} \n",
    "x_1^1 & \\ldots & x_n^1 \\\\\n",
    "x_1^2 & \\ldots & x_n^2 \\\\\n",
    "\\vdots &\\vdots &\\vdots \\\\\n",
    "x_1^m & \\ldots & x_n^m \\\\\n",
    "\\end{pmatrix}\n",
    "\\text{   and   }\n",
    "X' = \\begin{pmatrix} \n",
    "1 & x_1^1 & \\ldots & x_n^1 \\\\\n",
    "1 & x_1^2 & \\ldots & x_n^2 \\\\\n",
    "\\vdots &\\vdots &\\vdots &\\vdots \\\\\n",
    "1 & x_1^m & \\ldots & x_n^m \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the logistic hypothesis using your implementation of the logistic function. `logistic_hypothesis` should return a function which accepts the training data $X$. Example usage:\n",
    "\n",
    "`>> theta = np.array([1.1, 2.0, -.9])`\n",
    "\n",
    "`>> h = logistic_hypothesis(theta) `\n",
    "\n",
    "`>> print(h(X))`\n",
    "\n",
    "**Note:** The training data was sampled with random noise, so the actual values of your h(X) may differ.\n",
    "\n",
    "`array([0.03587382, 0.0299963 , 0.97389774, ...,`\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "You may of course also implement a helper function for transforming $X$ into $X'$ and use it inside the `lamda` function of `logistic_hypothesis`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def logistic_hypothesis(theta):\n",
    "    ''' Combines given list argument in a logistic equation and returns it as a function\n",
    "    \n",
    "    Args:\n",
    "        thetas: list of coefficients\n",
    "        \n",
    "    Returns:\n",
    "        lambda that models a logistc function based on thetas and x\n",
    "    '''\n",
    "    raise NotImplementedError(\"You should implement this function\")\n",
    "\n",
    "    \n",
    "### Uncomment to test your implementation\n",
    "#theta = np.array([1.,2.,3.])\n",
    "#h = logistic_hypothesis(theta)\n",
    "#print(h(X))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cross-entropy\n",
    "\n",
    "The cross-entropy costs are defined with:\n",
    "\n",
    "\\begin{equation}\n",
    "    loss(h_\\theta (x^i), y^i) = -y^i \\cdot log(h_\\theta (x^i)) - (1-y^i) \\cdot log(1-h_\\theta(x^i))\n",
    "\\end{equation}\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the cross-entropy cost. \n",
    "\n",
    "Your python function should return a function, which accepts the vector $\\vec \\theta$.\n",
    "The returned function should return the cost for each feature vector $\\vec x^i$. The length of the returned array of costs therefore has to be the same length as we have feature vectors (and also labels $y$). Example usage:\n",
    "\n",
    "`>> J = cross_entropy_loss(logistic_hypothesis, X, y)`\n",
    "\n",
    "`>> print(J(theta))`\n",
    "\n",
    "**Note:** The training data was sampled with random noise, so the actual values of your h(X) may differ.\n",
    "\n",
    "\n",
    "`array([ 7.3,  9.5, ....`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cross_entropy_costs(h, X, y):\n",
    "    ''' Implements cross-entropy as a function costs(theta) on given traning data \n",
    "    \n",
    "    Args:\n",
    "        h: the hypothesis as function\n",
    "        x: features as 2D array with shape (m_examples, n_features)  \n",
    "        y: ground truth labels for given features with shape (m_examples)\n",
    "        \n",
    "    Returns:\n",
    "        lambda costs(theta) that models the cross-entropy for each x^i\n",
    "    '''\n",
    "    raise NotImplementedError(\"You should implement this function\")\n",
    "\n",
    "### Uncomment to test your implementation\n",
    "#theta = np.array([1.,2.,3.])\n",
    "#costs = cross_entropy_costs(logistic_hypothesis, X, y)\n",
    "#print(costs(theta))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loss Function\n",
    "\n",
    "\\begin{equation}\n",
    "    J_D(\\theta)=\\frac{1}{m}\\sum_{i=1}^{m}\\left(loss(h_\\theta (x^i), y^i)\\right)\n",
    "\\end{equation}\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Now implement the loss function $J$, which calculates the mean costs for the whole training data $X$. Your python function should return a function, which accepts the vector $\\vec \\theta$.\n",
    "\n",
    "**Note:** You can ignore the parameter `lambda_reg` for now, it is a hyperparameter for regularization. In a later exercise, you may revisit your implementation and implement regularization if you wish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "def mean_cross_entropy_costs(X, y, hypothesis, cost_func, lambda_reg=0.1):\n",
    "    ''' Implements mean cross-entropy as a function J(theta) on given traning data \n",
    "    \n",
    "    Args:\n",
    "        X: features as 2D array with shape (m_examples, n_features)  \n",
    "        y: ground truth labels for given features with shape (m_examples)\n",
    "        hypothesis: the hypothesis as function\n",
    "        cost_func: cost function\n",
    "        \n",
    "    Returns:\n",
    "        lambda J(theta) that models the mean cross-entropy\n",
    "    '''\n",
    "    raise NotImplementedError(\"You should implement this\")\n",
    "    \n",
    "### Uncomment to test your implementation\n",
    "#theta = np.array([1.,2.,3.])\n",
    "#J = mean_cross_entropy_costs(X,y, logistic_hypothesis, cross_entropy_costs, 0.1)\n",
    "#print(J(theta))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "###  Gradient Descent\n",
    "\n",
    "A short recap, the gradient descent algorithm is a first-order iterative optimization for finding a minimum of a function. From the current position in a (cost) function, the algorithm steps proportional to the negative of the gradient and repeats this until it reaches a local or global minimum and determines. Stepping proportional means that it does not go entirely in the direction of the negative gradient, but scaled by a fixed value $\\alpha$ also called the learning rate. Implementing the following formalized update rule is the core of the optimization process:\n",
    "\n",
    "\\begin{equation}\n",
    "    \\theta_{j_{new}} \\leftarrow \\theta_{j_{old}} - \\alpha * \\frac{\\delta}{\\delta\\theta_{j_{old}}} J(\\theta_{old})\n",
    "\\end{equation}\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the function to update all theta values.\n",
    "\n",
    "**Note:** You can ignore the parameter `lambda_reg` for now, it is a hyperparameter for regularization. In a later exercise, you may revisit your implementation and implement regularization if you wish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_new_theta(X, y, theta, learning_rate, hypothesis, lambda_reg=0.1):\n",
    "    ''' Updates learnable parameters theta \n",
    "    \n",
    "    The update is done by calculating the partial derivities of \n",
    "    the cost function including the linear hypothesis. The \n",
    "    gradients scaled by a scalar are subtracted from the given \n",
    "    theta values.\n",
    "    \n",
    "    Args:\n",
    "        X: 2D numpy array of x values\n",
    "        y: array of y values corresponding to x\n",
    "        theta: current theta values\n",
    "        learning_rate: value to scale the negative gradient  \n",
    "        hypothesis: the hypothesis as function\n",
    "\n",
    "        \n",
    "    Returns:\n",
    "        theta: Updated theta_0\n",
    "    '''\n",
    "    raise NotImplementedError(\"You should implement this\")\n",
    "\n",
    "### Uncomment to test your implementation\n",
    "theta = np.array([1.,2.,3.])\n",
    "theta = compute_new_theta(X, y, theta, .1, logistic_hypothesis, .1)\n",
    "print(theta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Using the `compute_new_theta` method, you can now implement the gradient descent algorithm. Iterate over the update rule to find the values for $\\theta$ that minimize our cost function $J_D(\\theta)$. This process is often called training of a machine learning model. \n",
    "\n",
    "**Task:**\n",
    "- Implement the function for the gradient descent.\n",
    "- Create a history of all theta and cost values and return them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gradient_descent(X, y, theta, learning_rate, num_iters, lambda_reg=0.1):\n",
    "    ''' Minimize theta values of a logistic model based on cross-entropy cost function\n",
    "    \n",
    "    Args:\n",
    "        X: 2D numpy array of x values\n",
    "        y: array of y values corresponding to x\n",
    "        theta: current theta values\n",
    "        learning_rate: value to scale the negative gradient  \n",
    "        num_iters: number of iterations updating thetas\n",
    "        lambda_reg: regularization strength\n",
    "        \n",
    "    Returns:\n",
    "        history_cost: cost after each iteration\n",
    "        history_theta: Updated theta values after each iteration\n",
    "    '''\n",
    "    raise NotImplementedError(\"You should implement this\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training and Evaluation\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Choose an appropriate learning rate, number of iterations and initial theta values and start the training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: Assign sensible values\n",
    "alpha = 42\n",
    "theta = np.array([42, -100, 10e5])\n",
    "num_iters = 1234\n",
    "history_cost, history_theta = gradient_descent(X, y, theta, alpha, num_iters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the training has finished we can visualize our results.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Plot the costs over the iterations. Your plot should look similar to this one:\n",
    "\n",
    "<img src=\"https://gitlab.com/deep.TEACHING/educational-materials/raw/dev/media/klaus/exercise-multivariate-linear-regression-costs.png\" width=\"512\" alt=\"internet connection needed\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def plot_progress(costs):\n",
    "    \"\"\" Plots the costs over the iterations\n",
    "    \n",
    "    Args:\n",
    "        costs: history of costs\n",
    "    \"\"\"\n",
    "    raise NotImplementedError(\"You should implement this!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_progress(history_cost)\n",
    "print(\"costs before the training:\\t \", history_cost[0])\n",
    "print(\"costs after the training:\\t \", history_cost[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot Data and Decision Boundary \n",
    "\n",
    "**Task:**\n",
    "\n",
    "Now plot the deicision boundary (a straight line in this case) together with the data.         "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Insert your code to plot below\n",
    "theta_hist[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Accuracy\n",
    "\n",
    "The logistic hypothesis outputs a value in the interval $]0;1[$. We want to map this value to one specific class i.e. $0$ or $1$, so we apply a threshold known as the decision boundary: If the predicted value is < 0.5, the class is 0, otherwise it is 1.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "1. Calculate the accuracy of your final classifier. The accuracy is the proportion of the correctly classified data.\n",
    "2. Why will the accuracy never reach 100% using this model and this data set?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Insert you code below\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Regularization\n",
    "\n",
    "**Task:**\n",
    "    \n",
    "Extend your implementation with a regularization term $\\lambda$ by adding it as argument to the functions `mean_cross_entropy_costs`, `compute_new_theta` and `gradient_descent`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "### Proof - Pen&Paper\n",
    "\n",
    "The sigmoid activation function is defined as $\\sigma (z) = \\frac{1}{1+\\exp(-z)}$ \n",
    "\n",
    "**Task:**\n",
    "\n",
    "Show that:\n",
    "$$\n",
    "\\frac{d \\sigma(z)}{d z} = \\sigma(z)(1-\\sigma(z))\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Now show that:\n",
    "$$\n",
    "\\frac{\\partial \\sigma(z)}{\\partial \\theta_1} = \\sigma(z)(1-\\sigma(z)) \\cdot x_1\n",
    "$$\n",
    "with:\n",
    "$$\n",
    "z = \\theta_0 x_0 + \\theta_1 x_1\n",
    "$$\n",
    "\n",
    "Note that in general (because of symmetry) holds:\n",
    "\n",
    "$$\n",
    "z = \\theta_0 x_0 + \\theta_1 x_1 + \\dots\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{\\partial \\sigma(z)}{\\partial \\theta_j} = \\sigma(z)(1-\\sigma(z)) \\cdot x_j\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Show from\n",
    "$$\n",
    "    \\frac{\\partial}{\\partial \\theta_j}  J(\\theta)  =  \n",
    "    \\frac{\\partial}{\\partial \\theta_j}  \\left( - \\frac{1}{m}  \\sum_{i=1}^{m} \n",
    "    \\left[  y^{(i)} \\log h_\\theta({\\vec x}^{(i)})+\n",
    "      (1 - y^{(i)}) \\log \\left( 1- h_\\theta({\\vec x}^{(i)})\\right) \\right] \\right)\n",
    "$$  \n",
    "that\n",
    "$$\n",
    "\\frac{\\partial}{\\partial \\theta_j}  J(\\theta)  =   \\frac{1}{m}\n",
    "     \\sum_{i=1}^{m} \\left( h_\\theta({\\vec x}^{(i)})- y^{(i)}\\right) x_j^{(i)}\n",
    "$$\n",
    "\n",
    "with the sigmoid function as hypothesis $h_\\theta(\\vec x^{(i)})$\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "Make use of your knowlede, that:\n",
    "$$\n",
    "\\frac{\\partial \\sigma(z)}{\\partial \\theta_j} = \\sigma(z)(1-\\sigma(z)) \\cdot x_j\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "During this exercise you learned about logistic regression and used it to perform binary classification on multidimensional data. You should be able to answer the following questions:\n",
    "* How can you interpret the output of the logistic function?\n",
    "* For which type of problem do you use linear regression and for which type of problem do you use logistic regression?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Exercise: Logistic Regression and Regularization <br/>\n",
    "by Christian Herta, Klaus Strohmenger <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
