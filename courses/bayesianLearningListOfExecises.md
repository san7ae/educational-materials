# Status: Draft

# Bayesian Learning

For the course content, see [Bayesian Learning](./bayesian-learning.pdf).


#### Prerequisite

* [exercise-expected-value](../notebooks/math/probability-theory/exercise-expected-value.ipynb)
* [exercise-variance-sample-size-dependence](../notebooks/monte-carlo-simulation/exercise-variance-sample-size-dependence.ipynb)
* [exercise-biased-monte-carlo-estimator](../notebooks/monte-carlo-simulation/exercise-biased-monte-carlo-estimator.ipynb)
* [exercise-entropy](../notebooks/information-theory/exercise-entropy.ipynb)
* [exercise-kullback-leibler-divergence](../notebooks/information-theory/exercise-kullback-leibler-divergence.ipynb)
* [exercise-multivariate-gaussian](../notebooks/math/probability-theory/distributions/exercise-multivariate-gaussian.ipynb)
* [exercise-bayes-rule](../notebooks/math/probability-theory/exercise-bayes-rule.ipynb)
* [exercise-jensen-inequality](../notebooks/math/probability-theory/exercise-jensen-inequality.ipynb)

#### (Maximum) likelihood and Maximum a posterior
* [exercise-univariate-gaussian-likelihood](../notebooks/machine-learning-fundamentals/exercise-univariate-gaussian-likelihood.ipynb)
* [exercise-linear-regression-MAP](../notebooks/machine-learning-fundamentals/exercise-linear-regression-MAP.ipynb)

#### Bayesian Networks
* [exercise-bayesian-networks-by-example](../notebooks/graphical-models/directed/exercise-bayesian-networks-by-example.ipynb) 
* [exercise-d-separation](../notebooks/graphical-models/directed/exercise-d-separation.ipynb) 
* [exercise-forward-reasoning-probability-tables](../notebooks/graphical-models/directed/exercise-forward-reasoning-probability-tables.ipynb) 
* [exercise-sensorfusion-and-kalman-filter-1d](../notebooks/sequence-learning/exercise-sensorfusion-and-kalman-filter-1d.ipynb)

#### EM-Algorithm
* [exercise-EM-simple-example](../notebooks/graphical-models/directed/exercise-EM-simple-example.ipynb)
* [exercise-1d-gmm-em](../notebooks/graphical-models/directed/exercise-1d-gmm-em.ipynb)
* [exercise-2d-gmm-em](../notebooks/graphical-models/directed/exercise-2D-gaussian-mixture-em.ipynb)


#### Monte-Carlo / MCMC / Sampling
* [exercise-inverse-transform-sampling](../notebooks/monte-carlo-simulation/sampling/exercise-sampling-inverse-transform.ipynb)
* [exercise-importance-sampling](../notebooks/monte-carlo-simulation/sampling/exercise-sampling-importance-new.ipynb)
* [exercise-rejection-sampling](../notebooks/monte-carlo-simulation/sampling/exercise-sampling-rejection.ipynb)
* [exercise-MCMC-Metropolis-sampling](../notebooks/monte-carlo-simulation/sampling/exercise-MCMC-Metropolis-sampling.ipynb)

#### Variance Reduction Techniques
* [exercise-variance-reduction-by-control-variates](../notebooks/monte-carlo-simulation/variance-reduction/exercise-variance-reduction-by-control-variates.ipynb)
* [exercise-variance-reduction-by-reparametrization](../notebooks/monte-carlo-simulation/variance-reduction/exercise-variance-reduction-by-reparametrization)
* [exercise-variance-reduction-via-rao-blackwellization](../notebooks/monte-carlo-simulation/variance-reduction/exercise-variance-reduction-via-rao-blackwellization)
* [exercise-variance-reduction-by-importance-sampling](../notebooks/monte-carlo-simulation/variance-reduction/exercise-variance-reduction-by-importance-sampling)

#### Variational Methods
* [exercise-variational-mean-field-approximation-for-a-simple-gaussian](../notebooks/variational/exercise-variational-mean-field-approximation-for-a-simple-gaussian)
* [exercise-variational-EM-bayesian-linear-regression](../notebooks/variational/exercise-variational-EM-bayesian-linear-regression)


#### Probabilistic Programming
* [exercise-pyro-simple-gaussian](../notebooks/probabilistic-programming/pyro/exercise-pyro-simple-gaussian.ipynb)
* [exercise-pymc3-examples](../notebooks/probabilistic-programming/pymc/exercise-pymc3-examples.ipynb)
* [exercise-pymc3-bundesliga-predictor](../notebooks/probabilistic-programming/pymc/exercise-pymc3-bundesliga-predictor.ipynb)
* [exercise-pymc3-ranking](../notebooks/probabilistic-programming/pymc/exercise-pymc3-ranking.ipynb)


#### Bayesian Deep Learning Examples
* For the exercises you need [dp.py](https://gitlab.com/deep.TEACHING/educational-materials/tree/dev/notebooks/differentiable-programming/dp.py).
* [exercise-variational-autoencoder](../notebooks/variational/exercise-variational-autoencoder.ipynb)
* [exercise-bayesian-by-backprop](../notebooks/variational/exercise-bayesian-by-backprop.ipynb)




<!--
**Coming soon**

**TODO:** Add introduction text.
**TODO:** Remove probabilistic-programming-notebooks, sampling-notebooks

## Notebooks
* [Exercise: Cookie Problem](../notebooks/machine-learning-fundamentals/probability-theory/exercise-cookie-problem)
* [exercise-softmax_regression-tensorflow](../notebooks/courses/htw-berlin/angewandte-informatik/advanced-topics/exercise-softmax_regression-tensorflow)
* [exercise-meaning-of-softmax-output-probability](../notebooks/courses/htw-berlin/angewandte-informatik/advanced-topics/exercise-meaning-of-softmax-output-probability)
* [exercise-natural-pairing](../notebooks/courses/htw-berlin/angewandte-informatik/advanced-topics/exercise-natural-pairing)
* [exercise-sampling-1](../notebooks/courses/htw-berlin/angewandte-informatik/advanced-topics/exercise-sampling-1)
* [exercise-pytorch-logistic-regression](../notebooks/courses/htw-berlin/angewandte-informatik/advanced-topics/exercise-pytorch-logistic-regression)
-->
