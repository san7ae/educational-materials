# Scenarios and Courses

This site contains a list of scenarios and courses.

The [project website](https://www.deep-teaching.org/) provides statically rendered HTML sites to preview the eductional materials. In order to interact with notebooks and to run code, clone the [educational-materials](https://gitlab.com/deep.TEACHING/educational-materials) Git repository to your own computer (see [How to use deep.TEACHING Notebooks](/#how-to-use-deepteaching-notebooks) for detailed instructions).

## Scenarios

A scenario is a real-world example. To solve the tasks in a scenario you might need to combine different machine learning techniques and and most likely also use different  libraries. Current scenarios are:

* [Medical Image Classification](courses/medical-image-classification.md)
* [Robotic Autonomous Driving](courses/robotic-autonomous-driving.md)
* [Natural Language Processing](courses/natural-language-processing.md)

## Courses

This section contains a list of courses. A course is defined as a set of notebooks, which have something in common, e.g:

* The notebooks in a course follow a didactic path. E.g. the course *Introduction to Machine Learning* aims to teach the fundamental core concepts of *neural networks*. It starts with python- and numpy-basics in order to teach *univariate linear regression*, evolves to *multivariate linear regression* and then classification via *logistic regression*.
* The notebooks in one course might be about a certain algorithm (e.g. logistic regression) and how to implement it using different libraries (e.g. numpy, tensorflow, pytorch, pymc).


Current courses are:

* [Introduction to Machine Learning](courses/introduction-to-ml.md)
* [Introduction to Neural Networks](courses/introduction-to-nns.md)
* [Convolutional Neural Networks](courses/cnns.md)
* [Bayesian Learning](courses/bayesian-learning.md)
* [Sequence Learning](courses/sequence-learning.md)
* [Differential Programming](courses/differential-programming.md)


## Prerequisites

Topics listed here are a usefull preparation to tackle the course and scenario notebooks given above.

* [Scientific NumPy](courses/scientific-numpy.md)
